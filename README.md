# Oceanic Spatialverse #



### What is Oceanic Spatialverse? ###

**Oceanic Spatialverse** (**OSV**) is a virtual 3D waterscape that explores the preservation, restoration, and utilization of the biome and biota of oceans.

**OSV** is an educational and alternative energy research platform for coastal and marine protection against pollution and climate change. 

**OSV** is a dynamic 3D representation of the world's marine biomes, which include oceans, coral reefs, and estuaries.
Biota (fish, seaweed, algae, etc.) and inanimate objects are presented as digital assets within an AR/VR environment.

**Oceanic Spatialverse** consists of the following components pertaining to the Ocean Re-CREATION Challenges listed below.

### CHALLENGE 1:  OCEAN POLLUTION & SEA LEVEL RISE ### 

* **OSV Story Maps** are narrative integrations of maps, data, and multimedia involving specific use cases of ocean conservation and sustainability.

* **OSV Locate** is a service that uses GIS and NLP-based localization to detect actual objects (e.g. ammunition residue) in the sea.

* **OSV Educate** will be a series of online courses about oceanography and marine biology.